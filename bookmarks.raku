#!/usr/bin/env raku
use v6.d;

constant $VERSION='11.2022';

my $browser = %*ENV<BROWSER> // '/usr/bin/librewolf'.IO;
my $bookmarks = "$*HOME/.newsboat/bookmarks.txt".IO;

sub main {
	if !@*ARGS || '-h'|'--help' eq @*ARGS[0] { help }
	elsif '-v'|'--version' eq @*ARGS[0] { $VERSION.Version.say }
	elsif @*ARGS[0] ~~ /^https?'://'/ { c_add @*ARGS[0] }
	elsif !$bookmarks.f { die "не знайдено файл $bookmarks" }
	elsif @*ARGS[0] ~~ /^(\-\-?)?s(how)?$/ {
		2 == @*ARGS ?? c_show @*ARGS[1] !! c_show
	} elsif @*ARGS[0] ~~ /^(\-\-?)?o(pen)?$/ {
		check_browser;
		if 1 == @*ARGS { die 'не вказано ід' }
		elsif 2 == @*ARGS && !+@*ARGS[1] { c_open "@*ARGS[1]" }
		else { c_open @*ARGS[1..*].map(+*||next) }
	} elsif @*ARGS[0] ~~ /^(\-\-?)?c(ount)?$/ {
		put +$bookmarks.lines~' '~$bookmarks
	} elsif @*ARGS[0] ~~ /^(\-\-?)?rm$/ {
		1 == @*ARGS
		&& prompt("Видалити $bookmarks? [т/н] ") eq 'т'
		?? (c_rm) !! c_rm @*ARGS[1..*].map(+*||next)
	}
}

sub c_add($_) {
	if $bookmarks.f && $_ eq any $bookmarks.lines { put 'вже є'; return }
	spurt $bookmarks,"$_\n",:append;
}

multi sub c_show { put $_.key+1~'. '~$_.value for $bookmarks.lines.pairs }
multi sub c_show($word) {
	put $_.key+1~'. '~$_.value
		for $bookmarks.lines.pairs.grep({$_.value~~/$word/})
}

multi sub c_open(@_) {
	my @urls = $bookmarks.lines;
	for @_.sort.unique.reverse {
		if $_ == none 1..@urls { put "пропущено $_"; next }
		run $browser,@urls[$_-1];
		put '-> видалено '~@urls.splice($_-1,1);
		save @urls
	}
}
multi sub c_open($word) {
	c_open $bookmarks.lines.pairs.grep({$_.value~~/$word/}).map(*.key+1)
}

multi sub c_rm { unlink $bookmarks }
multi sub c_rm(@_) {
	my @urls = $bookmarks.lines;
	for @_.sort.unique.reverse {
		if $_ == none 1..@urls { put "пропущено $_"; next }
		put '- видалено '~@urls.splice($_-1,1)
	}
	save @urls
}

sub check_browser {
	die "не знайдено $browser" if !$browser.f;
	die "запусти $browser" if run(<pidof>,'-q',$browser.basename).exitcode;
}

sub save(@_) { @_ ?? spurt $bookmarks,@_.join("\n")~"\n" !! unlink $bookmarks }

sub help {
	print qq:to/END/;
	Викор.: ./bookmarks <url>
		./bookmarkls [команди]

	-s,--show [слово]       показати список
	-o,--open [ід..|слово]  відкрити закладки
	-rm [ід..]              видалити закладки
	-v,--version            версія
	-h,--help               довідка

	(c) 2022 Андрій Мізик
	END
}

main
